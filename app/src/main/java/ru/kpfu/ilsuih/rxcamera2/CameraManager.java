package ru.kpfu.ilsuih.rxcamera2;

import android.view.View;

import java.io.File;

import javax.microedition.khronos.opengles.GL10;

import rx.Observable;
import rx.Subscriber;
import rx.subjects.AsyncSubject;

public class CameraManager {

    private MainView view;

    private Subscriber<? super GL10> subscriber;

    private CameraDelegate delegate;

    AsyncSubject<File> mFileObservable;

    CameraManager(MainView view) {
        this.view = view;
        delegate = new CameraDelegate(view.mRenderer.mSTexture, view.getContext());
        view.mRenderer.setCameraDelegate(delegate);
        view.mRenderer.setCameraManager(this);
    }

    View getView() {
        return view;
    }

    Observable<GL10> renderer() {
        return Observable.create(new Observable.OnSubscribe<GL10>() {

            @Override
            public void call(Subscriber<? super GL10> subscriber) {
                CameraManager.this.subscriber = subscriber;
            }
        });
    }

    void onFrame(GL10 es) {
        if (subscriber != null)
            subscriber.onNext(es);
    }

    Observable<File> shot() {
        mFileObservable = AsyncSubject.create();
        delegate.lockFocus(mFileObservable);
        return mFileObservable;
    }

    VideoManager startVideo() {
        delegate.startRecordingVideo();
        return new VideoManager(delegate);
    }

    void release() {
        delegate.closeCamera();
    }

}
