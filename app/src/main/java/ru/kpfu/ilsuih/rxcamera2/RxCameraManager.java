package ru.kpfu.ilsuih.rxcamera2;

import android.app.Activity;
import android.app.Fragment;

import rx.Observable;
import rx.Subscriber;

public class RxCameraManager {

    private static final String DUMMY_FRAGMENT_TAG = "DUMMY_FRAGMENT_TAG";
    public static RxCameraManager instance = null;
    private CameraManager cameraManager;
    private Activity mActivity;
    final MainView mainView;
    final DummyFragment fragment;

    public RxCameraManager(Activity context) {
        this.mActivity = context;
        mainView = new MainView(context);
        cameraManager = new CameraManager(mainView);
        Fragment previousFragment = mActivity.getFragmentManager().findFragmentByTag(DUMMY_FRAGMENT_TAG);
        if (previousFragment != null) {
            fragment = (DummyFragment) previousFragment;
        } else {
            fragment = new DummyFragment();
            mActivity.getFragmentManager().beginTransaction().add(fragment, DUMMY_FRAGMENT_TAG).commit();
        }
        fragment.setView(mainView);
    }

    public static Observable<CameraManager> with(Activity context) {
        if (instance == null)
            instance = new RxCameraManager(context);
        return  Observable.create(new Observable.OnSubscribe<CameraManager>() {
            @Override
            public void call(Subscriber<? super CameraManager> subscriber) {

                subscriber.onNext(instance.cameraManager);
            }
        });
    }

}


