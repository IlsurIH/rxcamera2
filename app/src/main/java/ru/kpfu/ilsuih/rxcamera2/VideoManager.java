package ru.kpfu.ilsuih.rxcamera2;

import java.io.File;

import rx.Observable;
import rx.subjects.AsyncSubject;

public class VideoManager {
    private CameraDelegate delegate;

    AsyncSubject<File> mFileObservable;

    public VideoManager(CameraDelegate delegate) {

        this.delegate = delegate;
    }

    public Observable<File> stop() {

        mFileObservable = AsyncSubject.create();
        delegate.stopRecordingVideo(mFileObservable);
        return mFileObservable;
    }
}
