package ru.kpfu.ilsuih.rxcamera2;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.io.File;

import rx.functions.Action1;

public class RxExampleActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        final RelativeLayout layout = new RelativeLayout(RxExampleActivity.this);
        RxCameraManager.with(this).subscribe(new Action1<CameraManager>() {
            @Override
            public void call(final CameraManager cameraManager) {
                layout.removeAllViews();
                layout.addView(cameraManager.getView());
                Button shotButton = new Button(RxExampleActivity.this);
                layout.addView(shotButton);

                shotButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        cameraManager.shot().subscribe(new Action1<File>() {
                            @Override
                            public void call(final File file) {
                                if (file.exists()) {
                                    new AlertDialog.Builder(v.getContext())
                                            .setTitle("Открыть файл в галерее?")
                                            .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent intent = new Intent();
                                                    intent.setAction(Intent.ACTION_VIEW);
                                                    intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "image/*");
                                                    startActivity(intent);
                                                }
                                            }).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create().show();
                                }
                            }
                        });
                    }
                });
            }
        });

        setContentView(layout);
    }
}
