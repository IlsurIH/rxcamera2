package ru.kpfu.ilsuih.rxcamera2;

import android.app.Fragment;

public class DummyFragment extends Fragment {

    private MainView mainView;

    public DummyFragment() {
    }

    public void setView(MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void onPause() {
        super.onPause();
        mainView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainView.onResume();
    }
}
